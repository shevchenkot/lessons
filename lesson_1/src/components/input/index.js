import React from "react";
import "./style.css";

export default class Input extends React.Component {
  render() {
    const { handleChange, type, placeholder, name } = this.props;

    return (
      <>
        <label htmlFor={name}>
          <b>{placeholder}</b>
        </label>
        <input
          className={`${type}Input`}
          onChange={handleChange}
          type={type}
          placeholder={placeholder}
          name={name}
          id={name}
          required
        />
      </>
    );
  }
}
