import React from "react";
import "./style.css";

export default class Button extends React.Component {
  render() {
    const { handleSubmit, type, className, text } = this.props;

    return (
      <button onClick={handleSubmit} type={type} className={className}>
        {text}
      </button>
    );
  }
}
