import React from "react";
import "./App.css";
import Button from "./components/button";
import Input from "./components/input";

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      psw: "",
      psw_repeat: null



    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  

  handleSubmit = (e) => {
    const { email, psw, psw_repeat } = this.state;

    if (email && psw === psw_repeat) {
      console.log(`Your email is ${email}, password is ${psw}`);
    } else {
      console.log(`Wrong data`);
    }

    e.preventDefault();
  };

  render() {
    console.log('hello')
    return (
      <form>
        <div className="container">
          <h1>Register</h1>
          <p>Please fill in this form to create an account.</p>
          <hr className="indent" />
          <Input
            handleChange={this.handleChange}
            type="text"
            placeholder="Email"
            name="email"
          />
          <Input
            handleChange={this.handleChange}
            type="password"
            placeholder="Password"
            name="psw"
          />
          <Input
            handleChange={this.handleChange}
            type="password"
            placeholder="Repeat Password"
            name="psw_repeat"
          />
          <hr className="indent" />
          <p>
            By creating an account you agree to our{" "}
            <a className="link" href="https://bit.do/YeetYeet">
              Terms & Privacy
            </a>
            .
          </p>
          <Button
            handleSubmit={this.handleSubmit}
            type="button"
            className="commonBtn"
            text="Register"
          />
        </div>

        <div className="container signin">
          <p>
            Already have an account?{" "}
            <a className="link" href="https://bit.do/YeetYeet">
              Sign in
            </a>
            .
          </p>
        </div>
      </form>
    );
  }
}
