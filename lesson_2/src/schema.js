const schema = {
  name: {
    rule: (val) => /^[A-Za-z ]+$/.test(val),
    errorText: "Wrong name",
  },
  email: {
    rule: (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val),
    errorText: "Invalid email addres",
  },
  psw: {
    rule: (val) => val.length > 5,
    errorText: "Wrong password",
  },
  psw_repeat: {
    rule: (val, { psw }) => val === psw,
    errorText: "Passwords should match",
  },
};

export default schema;
