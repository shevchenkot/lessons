import React, { useContext } from "react";
import "./auth.css";
import Button from "../button";
import { MyContext } from "../../my-context";

const Auth = () => {
  const { values, validate } = useContext(MyContext);

  const handleSubmit = () => {
    validate(async (isValid, errors) => {
      if (isValid) {
        let newUser = await fetch(
          "https://jsonplaceholder.typicode.com/users",
          {
            method: "POST",
            values,
          }
        ).then((val) => val.json());

        console.log(`User was created with id = ${newUser.id}`);
      } else {
        console.log(Object.values(errors));
      }
    });
  };

  return (
    <>
      <div className="container signin">
        <p>
          Already have an account?{" "}
          <a className="link" href="https://bit.do/YeetYeet">
            Sign in
          </a>
          .
        </p>
      </div>
      <Button
        handleSubmit={handleSubmit}
        type="button"
        className="commonBtn"
        text="Register"
      />
    </>
  );
};

export default Auth;
