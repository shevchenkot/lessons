import React from "react";
import "./button.css";

const Button = (props) => {
  const { handleSubmit, type, className, text } = props;

  return (
    <button
      onClick={handleSubmit}
      type={type}
      className={className}
    >
      {text}
    </button>
  );
};

export default Button;
