import React from "react";
import "./input.css";

const Input = (props) => {
  const { type, placeholder, name, handleChange } = props;

  return (
    <label htmlFor={name}>
      <b>{placeholder}</b>
      <input
        className={`${type}Input`}
        onChange={handleChange}
        type={type}
        placeholder={placeholder}
        name={name}
        id={name}
      />
    </label>
  );
};

export default Input;
