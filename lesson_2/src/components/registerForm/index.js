import React, { useContext } from "react";
import Input from "../input";
import { MyContext } from "../../my-context";
import "./registerForm.css";

const RegisterForm = () => {
  const { setNewValue, errors } = useContext(MyContext);
  const inputsList = [
    { type: "text", placeholder: "Name", name: "name" },
    { type: "text", placeholder: "Email", name: "email" },
    { type: "password", placeholder: "Password", name: "psw" },
    { type: "password", placeholder: "Repeat Password", name: "psw_repeat" },
  ];

  return (
    <form>
      <div className="container">
        <h1>Register</h1>
        <p>Please fill in this form to create an account.</p>
        <hr className="indent" />
        {inputsList.map((field) => (
          <>
            <Input
              key={field.name}
              type={field.type}
              placeholder={field.placeholder}
              name={field.name}
              handleChange={setNewValue}
            />
            {errors[field.name] && (
              <div style={{ color: "red" }}>{errors[field.name]}</div>
            )}
          </>
        ))}
        <hr className="indent" />
        <p>
          By creating an account you agree to our{" "}
          <a className="link" href="https://bit.do/YeetYeet">
            Terms & Privacy
          </a>
          .
        </p>
      </div>
    </form>
  );
};

export default RegisterForm;
