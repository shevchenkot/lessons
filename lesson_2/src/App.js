import React from "react";
import "./App.css";
import Auth from "./components/auth";
import { MyContextProvider } from "./my-context";
import RegisterForm from "./components/registerForm";

const App = () => {
  const initialValues = {
    name: "",
    email: "",
    psw: "",
    psw_repeat: "",
  };
  return (
    <MyContextProvider initialValues={initialValues}>
      <RegisterForm />
      <Auth />
    </MyContextProvider>
  );
};

export default App;
