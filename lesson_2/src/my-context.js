import React, { useState } from "react";
import schema from "./schema";

export const MyContext = React.createContext({});

const Provider = ({ children, initialValues = {} }) => {
  const [values, setValues] = useState(initialValues);
  const [errors, setErrors] = useState({});
  const [isValid, setValid] = useState(false);

  const setNewValue = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const validateBySchema = () => {
    const invalidFields = {};

    Object.keys(values).forEach((key) => {
      if (!schema[key].rule(values[key], values))
        invalidFields[key] = schema[key].errorText;
    });
    setErrors(invalidFields);

    return invalidFields;
  };

  const validate = (callBack) => {
    const validateResult = validateBySchema();
    const isValidTrue = Object.keys(validateResult).length === 0;

    setValid(isValidTrue);
    callBack(isValidTrue, validateResult);
  };

  return (
    <MyContext.Provider value={{ validate, setNewValue, errors, isValid }}>
      {children}
    </MyContext.Provider>
  );
};

export const MyContextProvider = Provider;
